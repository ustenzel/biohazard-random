import Bio.Align
import Bio.Prelude

import qualified Data.ByteString.Char8      as S
import qualified Data.ByteString.Lazy.Char8 as L

main :: IO ()
main = getArgs >>= main'

main' :: [String] -> IO ()
main' [f1, f2] = do
    (h1:b1) <- return . S.lines =<< S.readFile f1
    (h2:b2) <- return . S.lines =<< S.readFile f2

    let (d, s1, s2) = myersAlign 3000 (S.concat b1) Globally (S.concat b2)
        ll = S.length (head b1)
        h' = S.concat [h1, S.pack ("--" ++ shows d "--"), S.tail h2, S.singleton '\n']

    S.hPut stdout h'
    L.hPut stdout $ L.unlines $ showAligned ll [s1,s2]

main' _ = do
    pn <- getProgName
    putStr $ "Usage: " ++ pn ++ " file1.fa file2.fa\n\
             \Computes the difference between two sequences.  Each input file should\n\
             \contain exactly one moderately sized DNA sequence, e.g. a mitochondrion.\n"
    exitFailure

