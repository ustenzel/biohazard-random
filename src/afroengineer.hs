{-# LANGUAGE CPP #-}
-- Cobble up a mitochondrion, or something similar.  This is not an
-- assembly, but something that could serve in stead of one :)
--
-- The goal is to reconstruct a mitochondrion (or similar small, haploid
-- locus) from a set of sequencing reads and a reference sequence.  The
-- idea is to first select reads using some sort of filtering strategy,
-- simply for speed reasons.  They are then aligned to the reference
-- using banded Smith-Waterman algorithm, and a more likely reference is
-- called.  This is repeated till it converges.  A bad implementation of
-- the idea was called MIA.

import Align
import SimpleSeed

import Bio.Bam
import Bio.Prelude                   hiding ( round, left, right )
import Bio.Streaming.Bytes                  ( gunzip )
import Control.Monad.Log
import System.Console.GetOpt
import System.Directory                     ( doesFileExist )

import qualified Bio.Streaming.Prelude      as Q
import qualified Data.ByteString.Char8      as S
import qualified Data.ByteString.Lazy.Char8 as L
import qualified Data.Sequence              as Z
import qualified Data.Vector.Generic        as V

-- | Reads a FastA file, drops the names, yields the sequences.
readFasta :: L.ByteString -> [( S.ByteString, [Either Nucleotides Nucleotides] )]
readFasta = go . dropWhile (not . isHeader) . L.lines
  where
    isHeader s = not (L.null s) && L.head s == '>'

    go [     ] = []
    go (hd:ls) = case break isHeader ls of
                (body, rest) -> let ns = map toNuc . concat $ map L.unpack body
                                    nm = S.concat . L.toChunks . L.tail . head $ L.words hd
                                in (nm,ns) : if null rest then [] else go rest

#if MIN_VERSION_biohazard(2,1,0)
    toNuc x | isUpper x = Right $ toNucleotides (c2w x)
            | otherwise = Left  $ toNucleotides (c2w (toUpper x))
#else
    toNuc x | isUpper x = Right $ toNucleotides x
            | otherwise = Left  $ toNucleotides (toUpper x)
#endif


-- | A query record.  We construct these after the seeding phase and
-- keep the bare minimum:  name, sequence/quality, seed region, flags
-- (currently only the strand).  Just enough to write a valig BAM file.

data QueryRec = QR { qr_name :: !Bytes           -- from BAM
                   , qr_seq  :: !QuerySeq        -- sequence and quality
                   , qr_pos  :: !RefPosn         -- start position of band
                   , qr_band :: !Bandwidth }     -- bandwidth (negative to indicate reversed sequence_
  deriving Show

data Conf = Conf {
    conf_references :: [FilePath] -> [FilePath],
    conf_aln_outputs :: Maybe (Int -> FilePath),
    conf_cal_outputs :: Maybe (Int -> FilePath) }

iniconf :: Conf
iniconf = Conf id Nothing Nothing

options :: [ OptDescr (Conf -> IO Conf) ]
options = [
    Option "r" ["reference"]  (ReqArg add_ref    "FILE") "Read references from FILE",
    Option "a" ["align-out"]  (ReqArg set_aln_out "PAT") "Write intermediate alignments to PAT",
    Option "c" ["called-out"] (ReqArg set_cal_out "PAT") "Write called references to PAT" ]
  where
    add_ref     f c = return $ c { conf_references = conf_references c . (:) f }
    set_aln_out p c = return $ c { conf_aln_outputs = Just (splice_pat p) }
    set_cal_out p c = return $ c { conf_cal_outputs = Just (splice_pat p) }

    splice_pat [] _ = []
    splice_pat ('%':'%':s) x = '%' : splice_pat s x
    splice_pat ('%':'d':s) x = shows x $ splice_pat s x
    splice_pat (c:s) x = c : splice_pat s x

main :: IO ()
main = do
    (opts, files, errors) <- getOpt Permute options <$> getArgs
    unless (null errors) $ mapM_ (hPutStrLn stderr) errors >> exitFailure
    Conf{..} <- foldl (>>=) (return iniconf) opts

    inputs@((refname,reference):_) <- concatMap readFasta <$> mapM L.readFile (conf_references [])

    let !sm = create_seed_maps (map (map (either id id) . snd) inputs)
        !rs = prep_reference reference

    let bamhdr = (mempty :: BamMeta)
                        { meta_hdr = BamHeader (1,0) Unsorted []
                        , meta_refs = Refs $ fromList [ BamSQ refname (length reference) [] ] }

    -- uhh.. termination condition?
    let round :: (MonadIO m, MonadMask m, MonadLog m)
              => Int -> Stream (Of (QueryRec, AlignResult)) m (RefSeq, [QueryRec]) -> m ()
        round 10 _    = return ()
        round  n s    = do let bamout = case conf_aln_outputs of
                                            Nothing -> Q.effects
                                            Just nf -> write_iter_bam (nf n) bamhdr
                           (newref, queries) <- bamout s
                           case conf_cal_outputs of Nothing -> return ()
                                                    Just nf -> write_ref_fasta (nf n) n newref
                           logStringLn $ "Round " ++ shows n ": Kept " ++ shows (length queries) " queries."
                           round (n+1) (roundN newref (Q.each queries))

    withLogging_ (LoggingConf Warning Notice Error 10 True) $ readFreakingInputs files $ round 1 . round1 sm rs


-- General plan:  In the first round, we read, seed, align, call the new
-- working sequence, and write a BAM file.  Then write the new working
-- sequence out.  In subsequent rounds, the seeding is skipped and the
-- sequences come from memory.
--
-- XXX the bandwidth is too low, definitely in round 1, probably in
-- subsequent rounds.

round1 :: MonadIO m
       => SeedMap -> RefSeq
       -> Stream (Of BamRec) m ()                        -- queries in
       -> Stream (Of (QueryRec, AlignResult))            -- BAM output
              m (RefSeq, [QueryRec])                     -- new reference & queries out
round1 sm rs = roundN rs . Q.concat . Q.map seed
  where
    seed br@BamRec{..}
        | low_qual  = Nothing
        | otherwise = case do_seed (refseq_len rs) sm br of
            Nothing                -> Nothing
            Just (a,b) | a >= 0    -> Just $ QR b_qname (prep_query_fwd br) (RP   a ) (BW   bw )
                       | otherwise -> Just $ QR b_qname (prep_query_rev br) (RP (-b)) (BW (-bw))
                where bw = b - a - V.length b_seq
      where
        low_qual = 2 * l1 < l2
        l2 = V.length b_seq
        l1 = maybe l2 (V.length . V.filter (> Q 10)) b_qual

roundN :: Monad m
       => RefSeq
       -> Stream (Of QueryRec) m ()                      -- queries in
       -> Stream (Of (QueryRec, AlignResult))            -- BAM output
              m (RefSeq, [QueryRec])                     -- new reference & queries out
roundN rs stream = do
    (rs', xtab) :> qry' <- Q.store mkref . Q.store collect . Q.filter good $ Q.map aln stream
    return (rs', reverse $ map (xlate xtab) qry')
  where
    gap_cost = 50           -- Hmm, better suggestions?
    pad = 8

    aln qr@QR{..} = let res = align gap_cost rs qr_seq qr_pos qr_band
                    in ( new_coords qr res, res )
    good (_, res) = viterbi_score res < 0

    mkref = Q.fold step (new_ref_seq rs) finalize_ref_seq
      where
        step nrs (qr, res) = add_to_refseq nrs (qr_seq qr) res

    collect :: Monad m => Stream (Of (QueryRec, AlignResult)) m () -> m [(Int,Int,QueryRec)]
    collect = Q.fold_ step [] id
      where
        step l (!qr,!ar) = (left,right,qr) : l
          where
            -- get alignment ends from ar, add some buffer
            -- XXX does this yield invalid coordinates?
            !left  = viterbi_position ar - 8
            !right = viterbi_position ar + 8 + alignedLength (viterbi_backtrace ar)

    xlate :: XTab -> (Int, Int, QueryRec) -> QueryRec
    xlate tab (l,r,qr) = case Z.viewr tab of
        Z.EmptyR                               ->  error "empty table"
        _ Z.:> newlen | r <= l                 ->  error "confused reft and light"
                      | left < 0 || right < 0  ->  error "too far left"
                      | right' < left          ->  error "flipped over"
                      | otherwise              ->  qr { qr_pos = RP left, qr_band = BW $ right' - left }
          where
            lk x | x < 0            = Z.index tab (x + Z.length tab - 1)
                 | x < Z.length tab = Z.index tab  x
                 | otherwise        = Z.index tab (x - Z.length tab + 1)

            left = lk l ; right = lk r
            right' = if left < right then right else right + newlen

    new_coords qr rr = qr { qr_pos  = RP $ viterbi_position rr - pad
                          , qr_band = BW $ (if reversed (qr_band qr) then negate else id) $
                                      2*pad + max_bandwidth (viterbi_backtrace rr) }

    reversed (BW x) = x < 0

    max_bandwidth = (+1) . (*2) . V.maximum . V.map abs . V.scanl plus 0

    plus a (Mat :* _) = a
    plus a (Ins :* n) = a+n
    plus a (Del :* n) = a-n
    plus _ (Nop :* _) = error "shouldn't happen"
    plus _ (SMa :* _) = error "shouldn't happen"
    plus _ (HMa :* _) = error "shouldn't happen"
    plus _ (Pad :* _) = error "shouldn't happen"



-- Outline for further rounds:  We keep the same queries, we use the new
-- reference called in the previous round.  Output is channelled to
-- different files.  However, we need to translate coordinates to keep
-- the alignment windows in the correct places.  This should actually
-- come from the calling of the new reference.  Note that coordinate
-- translation may actually change the bandwidth.  Also we have to
-- compute a sensible bandwidth from the alignment.

write_iter_bam :: (MonadIO m, MonadMask m) => FilePath -> BamMeta -> Stream (Of (QueryRec, AlignResult)) m r -> m r
write_iter_bam fp hdr = writeBamFile fp hdr . Q.map conv
  where
    conv (QR{..}, AlignResult{..}) = BamRec
            { b_qname           = qname
            , b_flag            = if reversed qr_band then flagReversed else 0
            , b_rname           = Refseq 0
            , b_pos             = viterbi_position
            , b_mapq            = Q 255
            , b_cigar           = viterbi_backtrace
            , b_mrnm            = invalidRefseq
            , b_mpos            = 0
            , b_isize           = 0
            , b_seq             = qseqToBamSeq qr_seq
            , b_qual            = Just $ qseqToBamQual qr_seq
            , b_virtual_offset  = 0
            , b_exts            = [] }
      where
        qname = qr_name `S.append` S.pack ("  " ++ showFFloat (Just 1) viterbi_score [])
        reversed (BW x) = x < 0

-- | Calls sequence and writes to file.  We call a base only if the gap
-- has a probability lower than 50%.  We call a weak base if the gap has
-- a probality of more than 25%.  If the most likely base is at least
-- twice as likely as the second most likely one, we call it.  Else we
-- call an N or n.
write_ref_fasta :: MonadIO m => FilePath -> Int -> RefSeq -> m ()
write_ref_fasta fp num rs = liftIO $ writeFile fp $ unlines $
    (">genotype_call-" ++ show num) : chunk 70 (ref_to_ascii rs)
  where
    chunk n s = case splitAt n s of _ | null s -> [] ; (l,r) -> l : chunk n r

ref_to_ascii :: RefSeq -> String
ref_to_ascii (RS v) = [ base | i <- [0, 5 .. V.length v - 5]
                             , let pgap = indexV "ref_to_ascii/pgap" v (i+4)
                             , pgap > 3
                             , let letters = if pgap <= 6 then "acgtn" else "ACGTN"
                             , let (ix, p1, p2) = minmin i 4
                             , let good = p2 - p1 >= 3 -- probably nonsense
                             , let base = S.index letters $ if good then ix else  trace (show (V.slice i 5 v)) 4 ]
  where
    minmin i0 l = V.ifoldl' step (l, 255, 255) $ V.slice i0 l v
    step (!i, !m, !n) j x | x <= m    = (j, x, m)
                          | x <= n    = (i, m, x)
                          | otherwise = (i, m, n)



readFreakingInputs :: (MonadIO m, MonadMask m, MonadLog m) => [FilePath] -> (Stream (Of BamRec) m () -> m b) -> m b
readFreakingInputs [] k = k (pure ())
readFreakingInputs (f:fs) k = readFreakingInput f $ \s -> readFreakingInputs fs $ \ss -> k (s >> ss)

readFreakingInput :: (MonadIO m, MonadMask m, MonadLog m) => FilePath -> (Stream (Of BamRec) m () -> m b) -> m b
readFreakingInput fp k | ".bam" `isSuffixOf` fp = do logStringLn $ "Reading BAM from " ++ fp
                                                     decodeBamFile fp $ \_hdr -> k . Q.map unpackBam
                       | otherwise              = maybe_read_two fp  k

-- Finds the fie with read 2:  If the filename contains "r1" anywhere,
-- and replacing it with "r2" yields the name of an existing files,
-- that's the one that contains read 2.
check_r2 :: FilePath -> IO (Maybe FilePath)
check_r2 = go [] . reverse
  where
    go acc ('1':'r':fp) = do let fp' = reverse fp ++ 'r' : '2' : acc
                             e <- doesFileExist fp'
                             return $ if e then Just fp' else Nothing
    go acc (c:fp) = go (c:acc) fp
    go  _  [    ] = return Nothing

maybe_read_two :: (MonadIO m, MonadMask m, MonadLog m)
               => FilePath -> (Stream (Of BamRec) m () -> m r) -> m r
maybe_read_two fp k = liftIO (check_r2 fp) >>= maybe rd1 rd2
  where
    rd1     = do logStringLn $ "Reading FastQ from " ++ fp
                 streamFile fp $ k . parseFastq . gunzip
    rd2 fp2 = do logStringLn $ "Reading FastQ from " ++ fp ++ " and " ++ fp2
                 streamFile fp $ \s1 ->
                   streamFile fp2 $ \s2 ->
                     k . Q.concat $ Q.zipWith unite_pairs (parseFastq (gunzip s1)) (parseFastq (gunzip s2))

unite_pairs :: BamRec -> BamRec -> [BamRec]
unite_pairs a b = [ a { b_flag = b_flag a .|. flagFirstMate }
                  , b { b_flag = b_flag b .|. flagSecondMate } ]

