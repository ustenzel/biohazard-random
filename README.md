biohazard-random
================

This is a collection of command line utilities that may or may not be a
good idea and that may or may not be completely broken:

* `afroengineer`:  a wanna-be assembler for mitochondria.  It would work
  by alternating between aligning sequences to a reference and calling a
  new reference sequence, until it converges to the true mitochondrial
  sequence, but it is sufficiently unfinished to be useless.

* `fadiff`:  aligns two DNA sequences and highlights the differences.
  It works well for moderately sized sequences that are highly similar,
  e.g. two human mitochondria or two Corona viruses.

* `mt-anno`:  annotates hominin mitochondria.  It aligns a mitochondrion
  to the human reference, then slaps on the human annotation.  The only
  service it provides is the translation of coordinates.  This is a
  monumentally stupid idea, but Molecular Biologists do this all the time
  and Molecular Biologists asked for it to be automated...

* `mt-ccheck`:  tests for contamination in Neanderthal data by examining
  the reads aligning to the mitochondrion.  It's an incomplete port of
  badly bitrotten C code and quite possibly broken.


Installation
------------

`biohazard-random` uses Cabal, the standard installation mechanism for
Haskell.  It depends on the `biohazard` library and additional stuff
from Hackage.  To install, follow these steps:

* install GHC (see http://haskell.org/ghc)
  and cabal-install (see http://haskell.org/cabal),
* `cabal update` (takes a while to download the current package list),
* `git clone https://ustenzel@bitbucket.org/ustenzel/biohazard-random.git`
* `cabal install biohazard-random/`

When done, on an unmodified Cabal setup, you will find the binaries in 
`${HOME}/cabal/bin`.  Cabal can install them in a different place, please 
refer to the Cabal documentation at http://www.haskell.org/cabal/ if 
you need that.  Sometimes, repeated installations and re-installations can result 
in a thoroughly unusable state of the Cabal package collection.  If you get error 
messages that just don't make sense anymore, please refer to 
http://www.vex.net/~trebla/haskell/sicp.xhtml; among other useful things, it 
tells you how to wipe a package database without causing more destruction.
